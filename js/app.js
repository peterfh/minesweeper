
const FLAG_OFF = 0;
const FLAG_ON = 1;
const FLAG_QUESTION = 2;

class MinesweeperCell {
	constructor(id, row, col) {
		this.id = id;
		this.row = row;
		this.col = col;
		this.hasMine = false;
		this.isOpened = false;
		this.flagState = FLAG_OFF;
		this.nearbyMines = 0;
	}

	increaseNearbyMines() {
		this.nearbyMines++;
	}

	setMine(hasMine) {
		this.hasMine = hasMine;
	}

	setFlagState(state) {
		this.flagState = state;
		this.updateHtml();
	}

	shiftFlagState() {
		if (this.isOpened) {
			return false;
		}

		switch (this.flagState) {
			case FLAG_OFF:
				this.setFlagState(FLAG_ON);
				break;
			case FLAG_ON:
				this.setFlagState(FLAG_QUESTION);
				break;
			case FLAG_QUESTION:
				this.setFlagState(FLAG_OFF);
				break;
		}

		return this.flagState;
	}

	reveal() {
		if (this.isOpened) {
			return false;
		}
		
		this.isOpened = true;
		this.updateHtml();

		return true;
	}

	canReveal() {
		return this.flagState == FLAG_OFF;
	}

	toHtml() {
		var output = document.createElement("div");
		output.setAttribute("id", this.id);
		output.setAttribute("class", "cell");
		output.setAttribute("data-row", this.row);
		output.setAttribute("data-col", this.col);

		return output;
	}

	updateHtml() {
		var cell = document.getElementById(this.id);

		// Opened
		if (this.isOpened) {
			cell.className += " opened";

			if (this.hasMine) {
				cell.className += " val-mine";
			} else {
				if (this.flagState == FLAG_ON) {
					cell.innerHTML = "X";
					cell.className += " val-error";
				} else {
					if (this.nearbyMines > 0) {
						cell.innerHTML = this.nearbyMines;
					}

					cell.className += " val-" + this.nearbyMines;
				}
			}
		// Closed
		} else {
			switch (this.flagState) {
				case FLAG_OFF:
					cell.innerHTML = "";
					break;
				case FLAG_ON:
					cell.innerHTML = "&#9873;";
					break;
				case FLAG_QUESTION:
					cell.innerHTML = "?";
					break;
			}
		}
	}
}


class MinesweeperField {
	constructor() {
		this.id = "game-field";
		this.scoreBoardId = "scoreboard";
		this.rows = 10;
		this.cols = 10;
		this.mines = 10;
		this.flags = 0;
		this.closed = this.rows * this.cols;
		this.field = [];
		this.cellOnClick = "";
		this.isFinished = false;
	}

	begin() {
		this.reset();
		this.addMines();
		this.draw();
		this.addEvents();
	}

	// Reset field
	reset() {
		this.flags = 0;
		this.closed = this.rows * this.cols;
		this.field = [];
		this.isFinished = false;
		
		for (var row = 0; row < this.rows; row++) {
			this.field[row] = [];

			for (var col = 0; col < this.cols; col++) {
				var cellId = "cell-" + row + "-" + col;
				this.field[row][col] = new MinesweeperCell(cellId, row, col);
			}
		}
	}

	// Add mines
	addMines() {
		var row, col;

		for (var mine = 0; mine < this.mines; mine++) {
			do {
				row = Math.floor(this.rows * Math.random());
				col = Math.floor(this.cols * Math.random());
			} while (this.field[row][col].hasMine);

			this.field[row][col].setMine(true);

			var neighbours = this.getNeighbours(row, col);

			for (var j = 0; j < neighbours.length; j++) {
				neighbours[j].increaseNearbyMines();
			}
		}
	}

	getNearbyFlags(row, col) {
		var flags = 0;
		var neighbours = this.getNeighbours(row, col);

		for (var j = 0; j < neighbours.length; j++) {
			if (neighbours[j].flagState == FLAG_ON) {
				flags++;
			}
		}

		return flags;
	}

	getNeighbours(row, col) {
		var neighbours = [];

		var rMin = -1, rMax = 1, cMin = -1, cMax = 1;

		if (row < 1) {
			rMin = 0;
		}

		if (row + 1 >= this.rows) {
			rMax = 0;
		}

		if (col < 1) {
			cMin = 0;
		}

		if (col + 1 >= this.cols) {
			cMax = 0;
		}

		for (var r = rMin; r <= rMax; r++) {
			for (var c = cMin; c <= cMax; c++) {
				if (r != 0 || c != 0) {
					neighbours.push(this.field[row + r][col + c]);
				}
			}
		}

		return neighbours;
	}

	hint() {
		var row0 = Math.floor(this.rows * Math.random());
		var col0 = Math.floor(this.cols * Math.random());

		for (var row = row0; row < this.rows; row++) {
			for (var col = col0; col < this.cols; col++) {
				if (!this.field[row][col].hasMine) {
					this.revealCell(row, col);

					return;
				}
			}
		}

		for (var row = 0; row < row0; row++) {
			for (var col = 0; col < col0; col++) {
				if (!this.field[row][col].hasMine) {
					this.revealCell(row, col);

					return;
				}
			}
		}
	}

	revealCell(row, col) {
		var cell = this.field[row][col];

		if (!cell.canReveal()) {
			return false;
		}

		if (cell.reveal()) {
			this.closed--;
		}

		// Mine
		if (cell.hasMine) {
			this.revealAll();
			this.gameOver(false);

			return false;
		}

		// Empty field
		if (cell.nearbyMines == 0) {
			var neighbours = this.getNeighbours(row, col);

			for (var j = 0; j < neighbours.length; j++) {
				if (!neighbours[j].isOpened) {
					this.revealCell(neighbours[j].row, neighbours[j].col);
				}
			}

			return true;
		}

		// Win
		if (this.closed == this.mines) {
			this.setMineFlags();
			this.gameOver(true);
		}

		return true;
	}

	revealCellNeighbours(row, col) {
		var cell = this.field[row][col];

		if (cell.isOpened && this.getNearbyFlags(row, col) == cell.nearbyMines) {
			var neighbours = this.getNeighbours(row, col);

			for (var j = 0; j < neighbours.length; j++) {
				if (!neighbours[j].isOpened) {
					this.revealCell(neighbours[j].row, neighbours[j].col);
				}
			}
		}	
	}

	setMineFlags() {
		for (var row = 0; row < this.rows; row++) {
			for (var col = 0; col < this.cols; col++) {
				if (this.field[row][col].hasMine) {
					this.field[row][col].setFlagState(FLAG_ON);
				}
			}
		}								
	}

	revealAll() {
		for (var row = 0; row < this.rows; row++) {
			for (var col = 0; col < this.cols; col++) {
				this.field[row][col].reveal();
			}
		}
	}

	shiftCellState(row, col) {
		var state = this.field[row][col].shiftFlagState();

		if (state === FLAG_ON) {
			this.flags++;
		} else if (state === FLAG_OFF) {
			this.flags--;
		}

		this.updateScoreboard();
								
		return 
	}

	gameOver(win) {
		this.isFinished = true;

		if (win) {
			alert("Has ganado! Has encontrado todas las minas.");
		} else {
			alert("Ha explotado una mina.");
		}
	}

	updateScoreboard() {
		var scoreboard = document.getElementById(this.scoreBoardId);
		scoreboard.innerHTML = this.mines - this.flags;
	}

	// Draw field
	draw() {
		var main = document.getElementById(this.id);
		main.innerHTML = "";

		for (var row = 0; row < this.rows; row++) {
			var rowTag = document.createElement("div");
			rowTag.setAttribute("id", "row-" + row);
			rowTag.setAttribute("data-row", row);
			rowTag.setAttribute("class", "row");

			for (var col = 0; col < this.cols; col++) {
				rowTag.appendChild(this.field[row][col].toHtml());
			}

			main.appendChild(rowTag);
		}

		this.updateScoreboard();
	}
	
	addEvents() {
		for (var row = 0; row < this.rows; row++) {
			for (var col = 0; col < this.cols; col++) {
				var cellTag = document.getElementById(this.field[row][col].id);
				cellTag.obj = this;
				cellTag.addEventListener("mouseup", this.cellOnClick = function(e) {
					MinesweeperField.cellClick(this.obj, e);
				}, false);
				cellTag.addEventListener("contextmenu", function(e) {
					e.preventDefault();
				}, false);
			}
		}
	}

	static cellClick(obj, e) {
		e.preventDefault();

		if (obj.isFinished) {
			return false;
		}

		var cell = document.getElementById(e.target.id);
		var rowId = Number(cell.getAttribute("data-row"));
		var colId = Number(cell.getAttribute("data-col"));

		switch (e.which) {
			// Left click
			case 1:
				obj.revealCell(rowId, colId);
				// cell.removeEventListener("mouseup", obj.cellOnClick);
				break;
			// Middle click
			case 2:
				obj.revealCellNeighbours(rowId, colId);
				break;
			// Righ click
			case 3:
				obj.shiftCellState(rowId, colId);
				break;
			default:
				alert('You have a strange Mouse!');					
		}
	}
}


// Main
var minesweeper = new MinesweeperField();

var begin = document.getElementById("begin");
begin.addEventListener("click", function(e) {
	minesweeper.begin(10, 10, 10);
}, false);
begin.click();

var revealAll = document.getElementById("reveal-all");
revealAll.addEventListener("click", function(e) {
	minesweeper.revealAll();
}, false);
